<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5 col-md-6 mx-auto">
        <div class="card-body p-0 ">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-md">
                    <div class="p-5">
                        <?= $this->session->flashdata('message'); ?>
                        <form class="user" method="post" action="<?=  base_url('auth/verify/') ?>">
                            <button type="submit" class="btn btn-primary btn-user btn-block mt-3">
                                Verify
                            </button>
                        </form>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
