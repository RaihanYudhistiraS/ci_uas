<?php
class Crud_model extends CI_Model{
  //get all categories method
  function get_category(){
      $result=$this->db->get('user');
      return $result;
  }
  //generate dataTable serverside method
  function get_all_product() {
      $this->datatables->select('id,name,email,qr,status');
      $this->datatables->from('user');
    //   $this->datatables->join('categories', 'product_category_id=category_id');
      $this->datatables->add_column('view', '<a href="javascript:void(0);" class="edit_record btn btn-info"  data-id="$1"  data-name="$2" data-email="$3" data-qr="$4" data-status="$5">Edit</a>  <a href="javascript:void(0);" class="delete_record btn btn-danger" data-id="$1">Delete</a>','id,name,email,qr,status');
      return $this->datatables->generate();
  }

  //insert data method
    function insert_product(){
        $data=array(
        'name'        => $this->input->post('name'),
        'email'        => $this->input->post('email'),
        );
        $result=$this->db->insert('user', $data);
        return $result;
    }
    //update data method
    function update_product(){
        $id=$this->input->post('id');
        $data=array(
            'name'        => $this->input->post('name'),
            'email'        => $this->input->post('email'),
        );
        $this->db->where('id',$id);
        $result=$this->db->update('user', $data);
        return $result;
    }
    //delete data method
    function delete_product(){
        $id=$this->input->post('id');
        $this->db->where('id',$id);
        $result=$this->db->delete('user');
        return $result;
    }
}
