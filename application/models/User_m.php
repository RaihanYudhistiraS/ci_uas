<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_m extends CI_Model
{
    //buat ambil data nama dari db buat nanti looping di view
    public function get()
    {
        $query = $this->db->get('user'); //select semua data di database dalam table user
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function submit()
    {
        $field = array(
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
        );
        $this->db->insert('user', $field);
    }

    //ambil data user dari paramter yang udah di pass di controller
    public function getID($id) {
        // $query = $this->db->get_where('user', array('id' => $id));
        $this->db->where('id', $id);
        $query = $this->db->get('user');

        if ($query->num_rows() > 0) {
            // return $query->result_array();
            return $query->row_array();
        } else {
            return false;
        }

    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('user');
    }


    //VERSI NON-SQL BIASA
    // public function update()
    // {
    //     $id = $this->input->post('id');//ambil id dari hidden field
    //     //data diambil dari form
    //     $field = array(
    //         'name' => $this->input->post('name'),
    //         'email' => $this->input->post('email')
    //     );

    //     $this->db->where('id', $id);
    //     $this->db->update('user',$field);
    // }

    public function update($field)
    {
        $sql = "UPDATE user SET name = ?, email = ? WHERE id = ?";
		$hsl = $this->db->query($sql, $field);
		return $hsl;
    }
}
