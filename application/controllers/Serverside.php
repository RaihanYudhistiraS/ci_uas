<?php
class Serverside extends CI_Controller
{

    function __construct(){
    parent::__construct();
        $this->load->model('User_m', 'm'); //parameter kedua hanya sebagai alias agar lebih mudah saat dipanggil
        $this->load->library('form_validation'); // library buat nanti dipake nambah data
        $this->load->library('user_agent');
        $this->load->library('ciqrcode');
        // SERVERSIDE
        $this->load->library('datatables'); //load library ignited-dataTable
        $this->load->model('crud_model'); //load model crud_model
    }
    function index(){
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array(); //ambil data nama user dari session
        $data['list'] = $this->m->get(); //ambil semua data dari table yang ada di database buat nanti di loop
        $data['browser'] = $this->agent->browser();
        $data['browserVersion'] = $this->agent->version();
        $data['platform'] = $this->agent->platform();
        $x['category']=$this->crud_model->get_category();

        $this->load->view('templates/header');
        $this->load->view('user/serverside_view', $data,$x);
        // $this->load->view('templates/footer');
    }

    function get_product_json() { //get product data and encode to be JSON object
        header('Content-Type: application/json');
        echo $this->crud_model->get_all_product();
    }

    function save(){ //insert record method
        $this->crud_model->insert_product();
        redirect('serverside');
    }

    function update(){ //update record method
        $this->crud_model->update_product();
        redirect('serverside');
    }

    function delete(){ //delete record method
        $this->crud_model->delete_product();
        redirect('serverside');
    }

}
