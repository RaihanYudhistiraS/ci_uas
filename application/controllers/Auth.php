<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct() //dipake vuar mmethod index dan register selalu pake ini
    {
        parent::__construct(); //manggil construct yang ada di CI_CONTROLLER
        $this->load->model('User_m', 'm'); //parameter kedua hanya sebagai alias agar lebih mudah saat dipanggil
        $this->load->model('users_model');
        $this->load->library('form_validation');
        $this->load->library('ciqrcode');
    }

    public function index()
    {
        //set rule untuk input di login
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
        $this->form_validation->set_rules('pass', 'Password', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header');
            $this->load->view('auth/login');
            $this->load->view('templates/footer');
        } else {
            $this->_login();
        }
    }

    private function _login()
    {
        $email = $this->input->post('email');
        $pass = $this->input->post('pass');
        //ambil data user
        $user = $this->db->get_where('user', ['email' => $email])->row_array();


        //cek ada atau gak usernya
        if ($user) {
            if (password_verify($pass, $user['password'])) {
                if($user['status'] == 1) {
                    //kalo ada ambil datanya
                    $data = [
                        'email' => $user['email'],
                        'name' => $user['name']
                    ];
                    //data disimpen di session dan redirect ke h alaman user
                    $this->session->set_userdata($data);
                    redirect(base_url('user/'));

                }else{
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Please Verify your account!</div>');
                }

            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Incorrect password!</div>');
                redirect(base_url());
            }
        }
        //kalo gaada user di database (belom register)
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">User not Registered!</div>');
        redirect(base_url());
    }


    public function register()
    {
        // recaptcha
        $recaptchaResponse = trim($this->input->post('g-recaptcha-response'));

        $userIp = $this->input->ip_address();

        $secret = $this->config->item('google_secret');

        $url="https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$recaptchaResponse."&remoteip=".$userIp;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        $status= json_decode($output, true);



        //setting rule form untuk masing-masing input di register
        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', [
            'is_unique' => 'Email already used'
        ]);
        $this->form_validation->set_rules('pass1', 'Password', 'required|trim|min_length[3]|matches[pass2]', [
            'matches' => 'password do not match',
            'min_length' => 'password too short'

        ]);
        $this->form_validation->set_rules('pass2', 'Password', 'required|trim|min_length[3]|matches[pass1]');
        //

        //kalo salah reload page
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header');
            $this->load->view('auth/register');
            $this->load->view('templates/footer');
            //kalo bener validasi form, cek recaptcha
        } else {
            // kalau recaptcha benar, ambil data terus masukkin ke db dan redirect ke login page
            if ($status['success']) {
                //QR
                $config['cacheable']    = true; //boolean, the default is true
                $config['cachedir']     = './assets/'; //string, the default is application/cache/
                $config['errorlog']     = './assets/'; //string, the default is application/logs/
                $config['imagedir']     = './assets/img/'; //direktori penyimpanan qr code
                $config['quality']      = true; //boolean, the default is true
                $config['size']         = '1024'; //interger, the default is 1024
                $config['black']        = array(224,255,255); // array, default is array(255,255,255)
                $config['white']        = array(70,130,180); // array, default is array(0,0,0)
                $this->ciqrcode->initialize($config);

                $image_name=$this->input->post('name').'.png'; //buat name dari qr code sesuai dengan nim

                $params['data'] = $this->input->post('email'); //data yang akan di jadikan QR CODE
                $params['level'] = 'H'; //H=High
                $params['size'] = 10;
                $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/img/
                $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE


                
                //get user inputs
                $name =  htmlspecialchars($this->input->post('name', true));
                $email = htmlspecialchars($this->input->post('email', true));
                $password = password_hash($this->input->post('pass1'), PASSWORD_DEFAULT);
                $qr =$image_name;
                //generate simple random code
                $set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $code = substr(str_shuffle($set), 0, 10);

                //insert user to users table and get id
                $user['name'] = $name;
                $user['email'] = $email;
                $user['password'] = $password;
                $user['code'] = $code;
                $user['qr'] = $qr;
                $user['status'] = false;
                $id = $this->users_model->insert($user);
                $e_email = base64_encode($email);


                $this->load->library('phpmailer_lib');

                // PHPMailer object
                $mail = $this->phpmailer_lib->load();

                // SMTP configuration
                $mail->SMTPDebug = 1;
                $mail->isSMTP();
                $mail->Host       = 'smtp.gmail.com';
                $mail->SMTPAuth   = true;
                $mail->Username   = 'heheh3h33@gmail.com';
                $mail->Password   = 'hahahihi';
                $mail->SMTPSecure = 'tls';
                $mail->Port       = 587;

                $mail->setFrom('heheh3h33@gmail.com','hoho');
                $mail->addAddress($this->input->post('email'));

                $mail->isHTML(true);



                $mail->Subject = 'Registration is Complete';
                $message ="
                        <html>
                        <head>
                            <title>Verification Code</title>
                        </head>
                        <body>
                            <h2>Thank you for Registering.</h2>
                            <p>Your Account:</p>
                            <p>Name: ".$name."</p>
                            <p>Email: ".$email."</p>

                            <p>Please click the link below to activate your account.</p>
                            <h4><a href='".base_url()."auth/verify/".$id."/".$code."".$e_email."'>Activate My Account</a></h4>
                        </body>
                        </html>
                        ";
                $mail->Body = $message;


                $mail->send();



                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Registered succesfully!</div>');
                redirect('auth');



            }else{
                // kalo recaptcha gagal
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">recaptcha failed</div>');
                $this->load->view('templates/header');
                $this->load->view('auth/register');
                $this->load->view('templates/footer');
            }
        }



    }

    public function verify()
    {
        $id =  $this->uri->segment(3);
    	$seg = $this->uri->segment(4);
        $code= substr($seg, 0,10);
        $en_email = substr($seg,10);


        $real_email =  base64_decode($en_email);
    		//fetch user details
    	$user = $this->users_model->getUser($id);

    		//if code matches
    	if($user['code'] == $code&&$user['email']==$real_email){
    	//update user active status
    		$data = true;
    		$query = $this->users_model->activate($data, $id);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Validation Success</div>');
    	}
    	else{
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Validation Failed</div>');
        }
    $this->load->view('templates/header');
    $this->load->view('auth/login');
    $this->load->view('templates/footer');
    }

}
