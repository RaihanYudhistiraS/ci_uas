<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

    public function __construct() //jalankan isi dari __construct ke semua method yang ada
    {
        parent::__construct();
        $this->load->model('User_m', 'm'); //parameter kedua hanya sebagai alias agar lebih mudah saat dipanggil
        $this->load->library('form_validation'); // library buat nanti dipake nambah data
        $this->load->library('user_agent');
    }

    public function index()
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array(); //ambil data nama user dari session
        $data['list'] = $this->m->get(); //ambil semua data dari table yang ada di database buat nanti di loop
        $data['browser'] = $this->agent->browser();
        $data['browserVersion'] = $this->agent->version();
        $data['platform'] = $this->agent->platform();

        $this->load->view('templates/header');
        $this->load->view('user/index', $data);
        $this->load->view('templates/footer');
    }

    public function submit()
    {
        $result = $this->m->submit();
        redirect(base_url('user/'));
    }

    public function edit($id)
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array(); //ambil data nama user dari session
        $data['change'] = $this->m->getID($id);//ambil id dari parameter url masukin ke model buat diambil datanya

        $this->load->view('templates/header');
        $this->load->view('user/edit', $data);
        $this->load->view('templates/footer');
    }

    public function delete($id)
    {

        $result = $this->m->delete($id);
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Deleted succesfully!</div>');
        redirect(base_url('user/'));
    }

    //VERSI NON-SQL BIASA
    // public function update()
    // {
    //     $result = $this->m->update();
    //     redirect(base_url('user/'));
    // }

    public function update()
    {
        //data diambil dari form
        $field = array(
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'id' => $this->input->post('id')
        );

        $this->m->update($field);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Modified succesfully!</div>');
        redirect(base_url('user/'));
    }

    public function add()
    {
        //setting rule form untuk masing-masing input di register
        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', [
            'is_unique' => 'Email already used'
        ]);
        $this->form_validation->set_rules('pass', 'Password', 'required|trim|min_length[3]', [
            'min_length' => 'password too short'
        ]);
        //

         //kalo salah reload page
         if ($this->form_validation->run() == false) {
            $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array(); //ambil data nama user dari session
            $this->load->view('templates/header');
            $this->load->view('user/add', $data);
            $this->load->view('templates/footer');
        } else {
            //kalo bener validasi form ambil data terus masukkin ke db dan redirect ke login page
            $form = [
                'name' => htmlspecialchars($this->input->post('name', true)),
                'email' => htmlspecialchars($this->input->post('email', true)),
                'password' => password_hash($this->input->post('pass'), PASSWORD_DEFAULT),
            ];

            $this->db->insert('user', $form);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Added succesfully!</div>');
            redirect(base_url('user/'));
        }
    }


    public function logout()
    {
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">You have logged out!</div>');
        session_destroy();
        redirect(base_url());
    }
}
